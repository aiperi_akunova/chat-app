import './App.css';
import {useEffect, useState} from "react";
import MessageBox from "./components/MessageBox/MessageBox";

const App = () => {

  const url = 'http://146.185.154.90:8000/messages';

  const [posts, setPosts] = useState([]);

  const [userPost, setUserPost] = useState({message : '', author: ''});


  useEffect(()=>{

    const fetchData = async () => {
      const response = await fetch(url);

      if (response.ok) {
        const posts = await response.json();
        setPosts(posts);
      }
    };

    fetchData().catch(e => console.error(e));
  }, [])


    const data = new URLSearchParams();
    data.set('message', userPost.message);
    data.set('author', userPost.author);

    const toPostMessage = ()=> {
      const fetchData = async () => {
        const response = await fetch(url, {
          method: 'post',
          body: data,
        });
      }
      fetchData().catch(e => console.error(e));
    }

    const inputMessageChange = (e)=>{
      const userPostCopy = {...userPost};
      userPostCopy.message = e.target.value;
      setUserPost(userPostCopy);
    }


  const inputAuthorChange = (e)=>{
      const userPostCopy = {...userPost};
      userPostCopy.author = e.target.value;
      setUserPost(userPostCopy);
  }

  return (
    <div className="App">
      <h1>Welcome to JS-10 chat!</h1>
      <div className='input'>
        <label>
          Message:
          <input
              autoComplete='off'
              type='text'
              value = {userPost.message}
              name = 'message'
              onChange={inputMessageChange}/>
        </label>

        <label>
          Author:
          <input
              type='text'
              autoComplete='off'
              value={userPost.author}
              name = 'author'
              onChange={inputAuthorChange}/>
        </label>
        <button onClick={toPostMessage}>Post</button>
      </div>
      <div className='main-box'>
        {posts.map(post=>
            <MessageBox
                title = {post.author}
                message = {post.message}
                key={post.id}
            />
        )}
      </div>

    </div>
  );
};

export default App;
